﻿using System.Security.Cryptography.Xml;

namespace SwiftReaderApi.Model
{
	public class TextBlock
	{
		public string TransactionReferenceNumber { get; set; }
		public string RelatedReference { get; set; }
		public string NarrativeText { get; set; }

	}
}
