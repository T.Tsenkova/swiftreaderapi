﻿using SwiftReaderApi.Model;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;

namespace SwiftReaderApi.Repository
{
	public class MessageRepository : IMessageRepository
	{
		private readonly SQLiteConnection _connection;

		public MessageRepository(SQLiteConnection connection)
		{
			_connection = connection;
		}

		public SQLiteConnection GetOpenConnection()
		{
			var connection = new SQLiteConnection(_connection.ConnectionString);
			connection.Open();
			return connection;
		}

		public void InsertMessage(Message message)
		{
			using (var connection = GetOpenConnection())
			using (var transaction = connection.BeginTransaction())
			{
				try
				{
					using (var command = connection.CreateCommand())
					{
						command.Transaction = transaction;

						command.CommandText = "INSERT INTO TextBlock (TransactionReferenceNumber, RelatedReference, NarrativeText) VALUES (@TransactionReferenceNumber, @RelatedReference, @NarrativeText)";
						command.Parameters.AddWithValue("@TransactionReferenceNumber", message.TextBlock.TransactionReferenceNumber);
						command.Parameters.AddWithValue("@RelatedReference", message.TextBlock.RelatedReference);
						command.Parameters.AddWithValue("@NarrativeText", message.TextBlock.NarrativeText);
						command.ExecuteNonQuery();

						long textBlockId = connection.LastInsertRowId;


						command.CommandText = "INSERT INTO TrailersBlock (TrailersBlockPart1, TrailersBlockPart2) VALUES (@TrailersBlockPart1, @TrailersBlockPart2)";
						command.Parameters.AddWithValue("@TrailersBlockPart1", message.TrailersBlock.MAC);
						command.Parameters.AddWithValue("@TrailersBlockPart2", message.TrailersBlock.CHK);
						command.ExecuteNonQuery();

						long trailersBlockId = connection.LastInsertRowId;

						command.CommandText = "INSERT INTO Messages (BasicHeader, AppHeader, UserHeader, TextBlockId, TrailersBlockId) VALUES (@BasicHeader, @AppHeader, @UserHeader, @TextBlockId, @TrailersBlockId)";
						command.Parameters.AddWithValue("@BasicHeader", message.BasicHeader);
						command.Parameters.AddWithValue("@AppHeader", message.AppHeader);
						command.Parameters.AddWithValue("@UserHeader", message.UserHeader);
						command.Parameters.AddWithValue("@TextBlockId", textBlockId);
						command.Parameters.AddWithValue("@TrailersBlockId", trailersBlockId);
						command.ExecuteNonQuery();

						transaction.Commit();
					}
				}
				catch (SqlException ex)
				{
					transaction.Rollback();
					Log.Error(ex, "SQL Exception occurred during transaction.");
				}
				catch (IOException ex)
				{
					transaction.Rollback();
					Log.Error(ex, "IO Exception occurred during transaction.");
				}
				catch (Exception ex)
				{
					transaction.Rollback();
					Log.Fatal(ex, "INCONSISTENT TRANSACTION!");
				}
			}
		}

		public IList<Message> GetAllMessages()
		{
			var messages = new List<Message>();

			using (var connection2 = GetOpenConnection())
			using (var command = connection2.CreateCommand())
			{
				command.CommandText = "SELECT * FROM Messages " +
					  "INNER JOIN TrailersBlock ON Messages.TrailersBlockId = TrailersBlock.Id " +
					  "INNER JOIN TextBlock ON Messages.TextBlockId = TextBlock.Id";

				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						TrailersBlock trailersBlock = new TrailersBlock()
						{
							MAC = reader.GetString(reader.GetOrdinal("TrailersBlockPart1")),
							CHK = reader.GetString(reader.GetOrdinal("TrailersBlockPart2"))
						};

						TextBlock textBlock = new TextBlock()
						{
							TransactionReferenceNumber = reader.GetString(reader.GetOrdinal("TransactionReferenceNumber")),
							RelatedReference = reader.GetString(reader.GetOrdinal("RelatedReference")),
							NarrativeText = reader.GetString(reader.GetOrdinal("NarrativeText"))

						};

						Message message = new Message
						{
							Id = reader.GetInt32(0),
							BasicHeader = reader.GetString(1),
							AppHeader = reader.GetValue(2).ToString(),
							UserHeader = reader.GetValue(3).ToString(),
							TextBlock = textBlock,
							TrailersBlock = trailersBlock
						};
						messages.Add(message);
					}
				}
			}

			return messages;
		}
	}
}
