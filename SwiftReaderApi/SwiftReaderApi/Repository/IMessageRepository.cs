﻿using SwiftReaderApi.Model;
using System.Data.SQLite;

namespace SwiftReaderApi.Repository
{
	public interface IMessageRepository
	{
		public SQLiteConnection GetOpenConnection();
		public void InsertMessage(Message message);
		public IList<Message> GetAllMessages();

	}
}
