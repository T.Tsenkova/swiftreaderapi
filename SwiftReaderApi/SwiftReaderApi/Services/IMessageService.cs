﻿using SwiftReaderApi.Model;

namespace SwiftReaderApi.Services
{
	public interface IMessageService
	{
		public Message StoreSwiftMessage(string swiftMessage);
		public Message ParseSwiftMessage(string swiftMessage);
		public IList<Message> GetAllMessages();

	}
}
