﻿using Microsoft.AspNetCore.DataProtection.KeyManagement;
using SwiftReaderApi.Model;
using SwiftReaderApi.Repository;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SwiftReaderApi.Services
{
	public class MessageService : IMessageService
	{
		private readonly IMessageRepository _messageRepository;
		private readonly string[] blocks = { "{1:", "{2:", "{3:", "{4:", "{5:" };
		private readonly string blockEnd = "}";
		private readonly string blockEndDouble = "}}";
		private const int num = 3;

		Dictionary<string, string> swiftMessageParced = new Dictionary<string, string>();

		public MessageService(IMessageRepository messageRepository)
		{
			_messageRepository = messageRepository;
		}

		public Message StoreSwiftMessage(string swiftMessage)
		{
			Message message = ParseSwiftMessage(swiftMessage);
			if (message != null)
			{
				_messageRepository.InsertMessage(message);
			}
			return message;
		}
		public Message ParseSwiftMessage(string swiftMessage)
		{
			for (int i = 0; i < blocks.Length; i++)
			{
				string block = blocks[i];
				if (swiftMessage.Contains(block))
				{
					string keyText = $"Block{i + 1}";
					BlockSplit(swiftMessage, block, keyText);
				}
			}

			Message parsedMessage = ParseMessageElements(swiftMessageParced);
			return parsedMessage;
		}

		public IList<Message> GetAllMessages()
		{
			var listOfMessages = _messageRepository.GetAllMessages();
			return listOfMessages;
		}

		private Dictionary<string, string> BlockSplit(string message, string start, string keyText)
		{
			string endPoint = blockEnd;
			string key = keyText;
			int cuts = num;
			if (start.Contains("5:"))
			{
				endPoint = blockEndDouble;
			}
			if (start.Contains("3:"))
			{
				cuts = num + 1;
			}

			int startIndex = message.IndexOf(start) + cuts;
			int endIndex = message.IndexOf(endPoint, startIndex);
			string result = message.Substring(startIndex, endIndex - startIndex);

			if (start.Contains("5:"))
			{
				Block5Split(result);
			}
			else if (start.Contains("4:"))
			{
				Block4Split(result);
			}
			else
			{
				swiftMessageParced.Add(key, result);
			}

			return swiftMessageParced;
		}

		private void Block5Split(string result)
		{
			string key = "TrailersBlock";
			string input = $"{result}}}";
			int counter = 1;
			string pattern = @"\{(.*?)\}";
			MatchCollection matches = Regex.Matches(input, pattern);

			foreach (Match match in matches)
			{
				string extractedValue = match.Groups[1].Value;
				swiftMessageParced.Add($"{key}{counter}", extractedValue);
				counter++;
			}
		}
		private void Block4Split(string result)
		{
			string key = "TextBlock";
			string input = result.Replace("\r", "").Replace("\n", "");
			if (input.Contains("\\r\\n"))
			{
				input = result.Replace("\\r\\n", "");
			}
			string[] fieldTags = { ":20:", ":21:", ":79:" };

			string[] messageParts = input.Split(fieldTags, StringSplitOptions.RemoveEmptyEntries);
			messageParts = messageParts.Where(part => !string.IsNullOrEmpty(part)).ToArray();

			for (int i = 0; i < messageParts.Length; i++)
			{
				string extractedValue = messageParts.Length > 0 ? fieldTags[i] + messageParts[i].Trim() : "";
				swiftMessageParced.Add($"{key}{i + 1}", extractedValue);
			}
		}


		private Message ParseMessageElements(Dictionary<string, string> elements)
		{
			var textBlock = new TextBlock
			{
				TransactionReferenceNumber = elements.GetValueOrDefault("TextBlock1"),
				RelatedReference = elements.GetValueOrDefault("TextBlock2"),
				NarrativeText = elements.GetValueOrDefault("TextBlock3"),
			};
			if (string.IsNullOrEmpty(textBlock.TransactionReferenceNumber) ||
				string.IsNullOrEmpty(textBlock.RelatedReference) ||
				string.IsNullOrEmpty(textBlock.NarrativeText))
			{
				return null;
			}
			var trailersBlock = new TrailersBlock
			{
				MAC = elements.GetValueOrDefault("TrailersBlock1"),
				CHK = elements.GetValueOrDefault("TrailersBlock2")
			};
			if (string.IsNullOrEmpty(trailersBlock.MAC) ||
				string.IsNullOrEmpty(trailersBlock.CHK))
			{
				return null;
			}
			Message message = new Message
			{

				BasicHeader = elements.GetValueOrDefault("Block1"),
				AppHeader = elements.GetValueOrDefault("Block2"),
				UserHeader = elements.GetValueOrDefault("Block3"),
				TextBlock = textBlock,
				TrailersBlock = trailersBlock
			};
			if (string.IsNullOrEmpty(message.BasicHeader) || (string.IsNullOrEmpty(message.AppHeader) && string.IsNullOrEmpty(message.UserHeader)))
			{
				return null;
			}
			return message;
		}

	}
}
