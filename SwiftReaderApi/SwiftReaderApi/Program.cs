global using Serilog;
using Microsoft.OpenApi.Models;
using SwiftReaderApi.Repository;
using SwiftReaderApi.Services;
using System.Data.SQLite;

namespace SwiftReaderApi
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var builder = WebApplication.CreateBuilder(args);

			// Add services to the container.
			builder.Services.AddScoped(provider =>
			{
				var connectionString = builder.Configuration.GetConnectionString("SQLite");
				return new SQLiteConnection(connectionString);
			});

			builder.Services.AddScoped<IMessageRepository, MessageRepository>();
			builder.Services.AddScoped<IMessageService, MessageService>();


			builder.Services.AddEndpointsApiExplorer();

			builder.Services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v2", new OpenApiInfo { Title = "My API", Version = "2.0" });
			});

			builder.Services.AddControllers();

			Log.Logger = new LoggerConfiguration()
				.ReadFrom.Configuration(builder.Configuration).CreateLogger();

			var app = builder.Build();

			if (app.Environment.IsDevelopment())
			{
				app.UseSwagger();
				app.UseSwaggerUI(c =>
				{
					c.SwaggerEndpoint("/swagger/v2/swagger.json", "My API v2");
				});
			}

			app.UseHttpsRedirection();

			app.UseAuthorization();

			app.MapControllers();

			app.Run();
		}
	}
}