﻿using Microsoft.AspNetCore.Mvc;
using SwiftReaderApi.Services;

namespace SwiftReaderApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class FileUploaderController : ControllerBase
	{
		private readonly IMessageService _messageService;

		public FileUploaderController(IMessageService messageService)
		{
			_messageService = messageService;
		}

		/// <summary>
		/// Recieves file with Swift message, reads file content, parse and save it in the dtabase
		/// </summary>
		[HttpPost("upload")]
		public async Task<IActionResult> UploadFile(IFormFile file)
		{
			if (file == null || file.Length <= 0)
			{
				return BadRequest("No file was uploaded.");
			}

			using (var reader = new StreamReader(file.OpenReadStream()))
			{
				var swiftMessage = await reader.ReadToEndAsync();
				var message = _messageService.StoreSwiftMessage(swiftMessage);
				if (message != null)
				{
					Log.Information("Message Saved via Uploaded File: => {@message}", message);
					return StatusCode(StatusCodes.Status200OK, message);
				}

				Log.Error("Incorrect file content");
				return StatusCode(StatusCodes.Status400BadRequest, "Incorrect file content");
			}
		}
	}
}
