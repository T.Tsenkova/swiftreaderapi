﻿using Microsoft.AspNetCore.Mvc;
using SwiftReaderApi.Model;
using SwiftReaderApi.Services;

namespace SwiftReaderApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class MessagesController : ControllerBase
	{
		private readonly IMessageService _messageService;
		public MessagesController(
			IMessageService messageService)
		{
			_messageService = messageService;
		}


		/// <summary>
		/// List all saved messages in the database
		/// </summary>
		[HttpGet]
		public IActionResult GetAllMessages()
		{
			var messages = _messageService.GetAllMessages();
			if (messages.Count > 0)
			{
				return StatusCode(StatusCodes.Status200OK, messages);
			}

			return StatusCode(StatusCodes.Status404NotFound, $"No messages");
		}

		/// <summary>
		/// Recieves string Swift message, parse and save it in the dtabase
		/// </summary>
		[HttpPost]
		public IActionResult Post([FromBody] string value)
		{
			var message = _messageService.StoreSwiftMessage(value);
			if (message != null)
			{
				Log.Information("Message Saved via Text/JSON: => {@message}", message);
				return StatusCode(StatusCodes.Status200OK, message);
			}

			Log.Error("Incorrect file content");
			return StatusCode(StatusCodes.Status400BadRequest, "Message Not Saved. Incorrect file content.");
		}
	}
}


