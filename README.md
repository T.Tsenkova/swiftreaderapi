# SwiftReaderApi

Web API for saving Swift MT799.

## Task

Web Api created with .NET 7 with main goal to receive Swift MT799 messages, parse the content and save the message in database.

- [ ] The app can receive messages as plain text or as files.
- [ ] If the message content is valid, it is parsed and stored in the database.
- [ ] Each processing attempt is logged in a log file.
- [ ] A list of all saved messages is also accessible.


## Tools and Params

- [ ] No external Swift messaging libraries are used.
- [ ] Documentation and testing environment provided via Swagger (accessible when the project is run).
- [ ] SQLite database and DB Browser for SQLite are utilized. The database used is embedded within the application files. 
- [ ] The database and tables are created and modified manually; Entity Framework is not used.
- [ ] Loger library used - Serilog.
- [ ] Test examples are also provided within the application files.


### Additionals
- [ ] REST principles are adhered to.
- [ ] A layered architecture is implemented.
- [ ] Data integrity is ensured.
